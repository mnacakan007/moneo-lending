import {Component, ElementRef, EventEmitter, OnInit, Output, ViewChild, Renderer} from '@angular/core';
import {
  bounceInDownOnEnterAnimation, bounceInLeftOnEnterAnimation,
  bounceInOnEnterAnimation,
  bounceInRightOnEnterAnimation,
  bounceInUpOnEnterAnimation
} from '../../../lib/bouncing-entrances';
import {
  bounceOnEnterAnimation,
  flashOnEnterAnimation, jelloOnEnterAnimation,
  pulseOnEnterAnimation,
  rubberBandOnEnterAnimation, shakeOnEnterAnimation, swingOnEnterAnimation, tadaOnEnterAnimation, wobbleOnEnterAnimation
} from '../../../lib/attention-seekers';
import {
  flipInXOnEnterAnimation,
  flipInYOnEnterAnimation,
  flipOnEnterAnimation,
  flipOutXOnLeaveAnimation,
  flipOutYOnLeaveAnimation
} from '../../../lib/flippers';
import {
  bounceOutDownOnLeaveAnimation,
  bounceOutLeftOnLeaveAnimation,
  bounceOutOnLeaveAnimation, bounceOutRightOnLeaveAnimation,
  bounceOutUpOnLeaveAnimation
} from '../../../lib/bouncint-exits';
import {
  fadeInDownBigOnEnterAnimation,
  fadeInDownOnEnterAnimation, fadeInLeftBigOnEnterAnimation,
  fadeInLeftOnEnterAnimation,
  fadeInOnEnterAnimation, fadeInRightBigOnEnterAnimation, fadeInRightOnEnterAnimation, fadeInUpBigOnEnterAnimation,
  fadeInUpOnEnterAnimation
} from '../../../lib/fading-entrances';
import {
  fadeOutDownBigOnLeaveAnimation,
  fadeOutDownOnLeaveAnimation, fadeOutLeftBigOnLeaveAnimation,
  fadeOutLeftOnLeaveAnimation,
  fadeOutOnLeaveAnimation, fadeOutRightBigOnLeaveAnimation, fadeOutRightOnLeaveAnimation, fadeOutUpBigOnLeaveAnimation,
  fadeOutUpOnLeaveAnimation
} from '../../../lib/fading-exits';
import {lightSpeedInOnEnterAnimation, lightSpeedOutOnLeaveAnimation} from '../../../lib/light-speed';
import {
  rotateInDownLeftOnEnterAnimation, rotateInDownRightOnEnterAnimation,
  rotateInOnEnterAnimation,
  rotateInUpLeftOnEnterAnimation,
  rotateInUpRightOnEnterAnimation
} from '../../../lib/rotating-entrances';
import {
  rotateOutDownLeftOnLeaveAnimation, rotateOutDownRightOnLeaveAnimation,
  rotateOutOnLeaveAnimation,
  rotateOutUpLeftOnLeaveAnimation,
  rotateOutUpRightOnLeaveAnimation
} from '../../../lib/rotating-exits';
import {
  slideInDownOnEnterAnimation,
  slideInLeftOnEnterAnimation,
  slideInRightOnEnterAnimation,
  slideInUpOnEnterAnimation
} from '../../../lib/sliding-entrances';
import {
  slideOutDownOnLeaveAnimation,
  slideOutLeftOnLeaveAnimation,
  slideOutRightOnLeaveAnimation,
  slideOutUpOnLeaveAnimation
} from '../../../lib/sliding-exits';
import {
  zoomInDownOnEnterAnimation,
  zoomInLeftOnEnterAnimation,
  zoomInOnEnterAnimation, zoomInRightOnEnterAnimation,
  zoomInUpOnEnterAnimation
} from '../../../lib/zooming-entrances';
import {
  zoomOutDownOnLeaveAnimation,
  zoomOutLeftOnLeaveAnimation,
  zoomOutOnLeaveAnimation, zoomOutRightOnLeaveAnimation,
  zoomOutUpOnLeaveAnimation
} from '../../../lib/zooming-exits';
import {hingeOnLeaveAnimation, jackInTheBoxOnEnterAnimation, rollInOnEnterAnimation, rollOutOnLeaveAnimation} from '../../../lib/specials';
import {
  collapseOnLeaveAnimation,
  expandOnEnterAnimation,
  fadeInExpandOnEnterAnimation,
  fadeOutCollapseOnLeaveAnimation
} from '../../../lib/other';

declare var $: any;

@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.scss'],
  animations: [
    bounceInRightOnEnterAnimation({ anchor: 'enter1' }),
    bounceInRightOnEnterAnimation({ anchor: 'enter2', delay: 100 }),
    bounceInRightOnEnterAnimation({ anchor: 'enter3', delay: 200, animateChildren: 'none' }),
    bounceInLeftOnEnterAnimation({ anchor: 'enter4' }),
    bounceInLeftOnEnterAnimation({ anchor: 'enter5', delay: 100 }),
    bounceOnEnterAnimation(),
    flashOnEnterAnimation(),
    pulseOnEnterAnimation(),
    rubberBandOnEnterAnimation(),
    shakeOnEnterAnimation(),
    swingOnEnterAnimation(),
    tadaOnEnterAnimation(),
    wobbleOnEnterAnimation(),
    jelloOnEnterAnimation(),
    flipOnEnterAnimation(),
    bounceInOnEnterAnimation(),
    bounceInUpOnEnterAnimation(),
    bounceOutOnLeaveAnimation(),
    bounceOutDownOnLeaveAnimation(),
    bounceInDownOnEnterAnimation(),
    bounceOutUpOnLeaveAnimation(),
    bounceInLeftOnEnterAnimation(),
    bounceInRightOnEnterAnimation(),
    bounceOutLeftOnLeaveAnimation(),
    bounceOutRightOnLeaveAnimation(),
    fadeInOnEnterAnimation(),
    fadeInUpOnEnterAnimation(),
    fadeInDownOnEnterAnimation(),
    fadeInLeftOnEnterAnimation(),
    fadeInRightOnEnterAnimation(),
    fadeInUpBigOnEnterAnimation(),
    fadeInDownBigOnEnterAnimation(),
    fadeInLeftBigOnEnterAnimation(),
    fadeInRightBigOnEnterAnimation(),
    fadeOutOnLeaveAnimation(),
    fadeOutUpOnLeaveAnimation(),
    fadeOutDownOnLeaveAnimation(),
    fadeOutLeftOnLeaveAnimation(),
    fadeOutRightOnLeaveAnimation(),
    fadeOutUpBigOnLeaveAnimation(),
    fadeOutDownBigOnLeaveAnimation(),
    fadeOutLeftBigOnLeaveAnimation(),
    fadeOutRightBigOnLeaveAnimation(),
    flipInXOnEnterAnimation(),
    flipInYOnEnterAnimation(),
    flipOutXOnLeaveAnimation(),
    flipOutYOnLeaveAnimation(),
    lightSpeedInOnEnterAnimation(),
    lightSpeedOutOnLeaveAnimation(),
    rotateInOnEnterAnimation(),
    rotateInUpLeftOnEnterAnimation(),
    rotateInUpRightOnEnterAnimation(),
    rotateInDownLeftOnEnterAnimation(),
    rotateInDownRightOnEnterAnimation(),
    rotateOutOnLeaveAnimation(),
    rotateOutUpLeftOnLeaveAnimation(),
    rotateOutUpRightOnLeaveAnimation(),
    rotateOutDownLeftOnLeaveAnimation(),
    rotateOutDownRightOnLeaveAnimation(),
    slideInRightOnEnterAnimation(),
    slideInUpOnEnterAnimation(),
    slideInDownOnEnterAnimation(),
    slideInLeftOnEnterAnimation(),
    slideOutUpOnLeaveAnimation(),
    slideOutDownOnLeaveAnimation(),
    slideOutLeftOnLeaveAnimation(),
    slideOutRightOnLeaveAnimation(),
    zoomInOnEnterAnimation(),
    zoomInUpOnEnterAnimation(),
    zoomInDownOnEnterAnimation(),
    zoomInLeftOnEnterAnimation(),
    zoomInRightOnEnterAnimation(),
    zoomOutOnLeaveAnimation(),
    zoomOutUpOnLeaveAnimation(),
    zoomOutDownOnLeaveAnimation(),
    zoomOutLeftOnLeaveAnimation(),
    zoomOutRightOnLeaveAnimation(),
    hingeOnLeaveAnimation(),
    jackInTheBoxOnEnterAnimation(),
    rollInOnEnterAnimation(),
    rollOutOnLeaveAnimation(),
    expandOnEnterAnimation({ duration: 400 }),
    collapseOnLeaveAnimation({ duration: 400 }),
    fadeInExpandOnEnterAnimation({ duration: 400 }),
    fadeOutCollapseOnLeaveAnimation({ duration: 400 })
  ]
})
export class CustomersComponent implements OnInit {

  @Output() onChanged = new EventEmitter<boolean>();
  @ViewChild('myDiv') myDiv: ElementRef;

  animation = 'bounceIn';
  state = true;

  change(increased: any) {
    this.onChanged.emit(increased);
    this.state = !this.state;
  }

  constructor() {}

  ngOnInit() {
    // setTimeout(() => {
    //   this.triggerFalseClick();
    // }, 5000);
   }


  // triggerFalseClick() {
  //   let el: HTMLElement = this.myDiv.nativeElement as HTMLElement;
  //   el.click();
  // }



}
