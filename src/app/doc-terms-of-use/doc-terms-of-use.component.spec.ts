import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocTermsOfUseComponent } from './doc-terms-of-use.component';

describe('DocTermsOfUseComponent', () => {
  let component: DocTermsOfUseComponent;
  let fixture: ComponentFixture<DocTermsOfUseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DocTermsOfUseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocTermsOfUseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
