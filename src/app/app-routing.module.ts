// import { StoreRouterConnectingModule } from "@ngrx/router-store";
// import { ServerErrorComponent } from './pages/server-error.component';
// import { PageNotFoundComponent } from './pages/page-not-found.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { BusinessComponent } from './business/business.component';
import { CustomersComponent } from './customers/customers.component';
import { MainComponent } from './main/main.component';
import { DocOfWhoWeAreComponent } from './doc-of-who-we-are/doc-of-who-we-are.component';
import { DocOfOurTechnologiesComponent } from './doc-of-our-technologies/doc-of-our-technologies.component';
import { DocOfRepresentativesComponent } from './doc-of-representatives/doc-of-representatives.component';
import { DocOfJobsComponent } from './doc-of-jobs/doc-of-jobs.component';
import { DocOfCollaborationComponent } from './doc-of-collaboration/doc-of-collaboration.component';
import { DocOfServiceComponent } from './doc-of-service/doc-of-service.component';
import { DocTermsOfUseComponent } from './doc-terms-of-use/doc-terms-of-use.component';
import { DocOfFranchisingComponent } from './doc-of-franchising/doc-of-franchising.component';
import { RegistrationComponent } from './registration/registration.component';
import { ContactComponent } from './contact/contact.component';

const routes: Routes = [
  { path: '', component: MainComponent,
    children: [
      { path: 'home', component: HomeComponent },
      { path: 'business', component: BusinessComponent },
      { path: 'customers', component: CustomersComponent },
    ],
  },
  { path: 'who', component: DocOfWhoWeAreComponent},
  { path: 'technologies', component: DocOfOurTechnologiesComponent},
  { path: 'representatives', component: DocOfRepresentativesComponent},
  { path: 'jobs', component: DocOfJobsComponent},
  { path: 'collaboration', component: DocOfCollaborationComponent},
  { path: 'service', component: DocOfServiceComponent},
  { path: 'terms', component: DocTermsOfUseComponent},
  { path: 'franchising', component: DocOfFranchisingComponent},
  { path: 'registration', component: RegistrationComponent},
  { path: 'contact', component: ContactComponent},

  // {path: '404', component: PageNotFoundComponent},
  // {path: '500', component: ServerErrorComponent},
  // {path: '**', component: PageNotFoundComponent},
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {scrollPositionRestoration: 'enabled'}),
    // StoreRouterConnectingModule,
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
