import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocOfWhoWeAreComponent } from './doc-of-who-we-are.component';

describe('DocOfWhoWeAreComponent', () => {
  let component: DocOfWhoWeAreComponent;
  let fixture: ComponentFixture<DocOfWhoWeAreComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DocOfWhoWeAreComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocOfWhoWeAreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
