import { Component, OnInit, ViewChild, ElementRef, Output, EventEmitter, TemplateRef } from '@angular/core';
import { Validators, FormGroup, FormBuilder, AbstractControl } from '@angular/forms';
import { Router } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';


export class PasswordValidation {

  static MatchPassword(AC: AbstractControl) {
    const password = AC.get('password').value; // to get value in input tag
    const confirmPassword = AC.get('confirmPassword').value; // to get value in input tag
    if(password !== confirmPassword) {
      AC.get('confirmPassword').setErrors( {MatchPassword: true} );
    } else {
      return null;
    }
  }
}

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})


export class RegistrationComponent implements OnInit {

  @ViewChild('scrollMe') private myScrollContainer: ElementRef;
  @Output() onChanged = new EventEmitter<boolean>();
  public formGroup: FormGroup;
  public submitted = false;
  public success = false;
  state = true;
  modalRef: BsModalRef;

  constructor(private fb: FormBuilder,
              private modalService: BsModalService,
              public router: Router) {}

  ngOnInit() {
    this.formGroup = this.fb.group({
      username: ['', Validators.required],
      company_legal_name: ['', Validators.required],
      password: ['', [Validators.required, Validators.minLength(6)]],
      brand: [''],
      confirmPassword: [''],
      country: ['', Validators.required],
      city: [''],
      address: ['', Validators.required],
      telephone: ['', Validators.required],
      website: [''],
      email: ['', [Validators.required, Validators.email]],
      company_tax_code: [''],
      contact_person: ['', Validators.required],
      position: ['', Validators.required],
    }, {
      validator: PasswordValidation.MatchPassword
    });

  }

  change(increased: any) {
    this.onChanged.emit(increased);
    this.state = !this.state;
  }

  get f() { return this.formGroup.controls; }

  onSubmit(template: TemplateRef<any>, model: any, isValid: boolean) {
    this.submitted = true;
    if (isValid) {
      this.success = true;
      console.log(model);
      this.modalRef = this.modalService.show(template, {class: 'modal-lg modal-dialog-centered'});
      setTimeout(() => {
        this.modalRef.hide();
        this.router.navigate(['/business']);
      }, 5000);
    }
  }

}
