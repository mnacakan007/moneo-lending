import { Component } from '@angular/core';

@Component({
  selector: 'app-page-not-found',
  template: `    
    <div class="message-block">
      <section id="not-found">
        <div id="title"><a [routerLink]="['/']">Navigate to main page</a></div>
        <div class="circles">
          <p>404<br>
            <small>PAGE NOT FOUND</small>
          </p>
          <span class="circle big"></span>
          <span class="circle med"></span>
          <span class="circle small"></span>
        </div>
      </section>
    </div>
  `,
  styleUrls: ['./page-not-found.component.scss']
})

export class PageNotFoundComponent {
}

