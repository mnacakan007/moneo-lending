import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ContentService {

  public change: boolean;

  constructor() {}

  changeActiveClass() {
    const header = document.getElementById('myDIV');
    const btns = header.getElementsByClassName('button-link');
    for (let i = 0; i < btns.length; i++) {
      btns[i].addEventListener('click', function () {
        const current = document.getElementsByClassName('active_button');
        current[0].className = current[0].className.replace(' active_button', '');
        this.className += ' active_button';
      });
    }
  }



}
