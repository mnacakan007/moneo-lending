import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocOfOurTechnologiesComponent } from './doc-of-our-technologies.component';

describe('DocOfOurTechnologiesComponent', () => {
  let component: DocOfOurTechnologiesComponent;
  let fixture: ComponentFixture<DocOfOurTechnologiesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DocOfOurTechnologiesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocOfOurTechnologiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
