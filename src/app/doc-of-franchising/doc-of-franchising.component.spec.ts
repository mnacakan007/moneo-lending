import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocOfFranchisingComponent } from './doc-of-franchising.component';

describe('DocOfFranchisingComponent', () => {
  let component: DocOfFranchisingComponent;
  let fixture: ComponentFixture<DocOfFranchisingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DocOfFranchisingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocOfFranchisingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
