import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocOfJobsComponent } from './doc-of-jobs.component';

describe('DocOfJobsComponent', () => {
  let component: DocOfJobsComponent;
  let fixture: ComponentFixture<DocOfJobsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DocOfJobsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocOfJobsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
