import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocOfCollaborationComponent } from './doc-of-collaboration.component';

describe('DocOfCollaborationComponent', () => {
  let component: DocOfCollaborationComponent;
  let fixture: ComponentFixture<DocOfCollaborationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DocOfCollaborationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocOfCollaborationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
