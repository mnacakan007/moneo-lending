export class List {
  constructor(
    public name: string,
    public text: string,
  ) {}
}

export interface Lists {
  lists: List[];
}
