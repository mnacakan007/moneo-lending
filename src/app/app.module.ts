import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

// import { ListsAction } from "./store/lists.action";
// import { StoreModule} from '@ngrx/store';
// import { StoreDevtoolsModule } from "@ngrx/store-devtools";
// import { listsReducer } from "../store/lists.reducer";
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { CustomersComponent } from './customers/customers.component';
import { BusinessComponent } from './business/business.component';
import { MainComponent } from './main/main.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AosToken, aos } from './aos';
import { ContentService } from './services/content.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap';
import { BsDropdownModule } from 'ngx-bootstrap';

import { PageNotFoundComponent } from './pages/page-not-found.component';
import { ServerErrorComponent } from "./pages/server-error.component";
import { DocOfWhoWeAreComponent } from './doc-of-who-we-are/doc-of-who-we-are.component';
import { DocOfOurTechnologiesComponent } from './doc-of-our-technologies/doc-of-our-technologies.component';
import { DocOfRepresentativesComponent } from './doc-of-representatives/doc-of-representatives.component';
import { DocOfJobsComponent } from './doc-of-jobs/doc-of-jobs.component';
import { DocOfCollaborationComponent } from './doc-of-collaboration/doc-of-collaboration.component';
import { DocOfServiceComponent } from './doc-of-service/doc-of-service.component';
import { DocTermsOfUseComponent } from './doc-terms-of-use/doc-terms-of-use.component';
import { DocOfFranchisingComponent } from './doc-of-franchising/doc-of-franchising.component';
import { RegistrationComponent } from './registration/registration.component';
import { ContactComponent } from './contact/contact.component';
import { LoadingSpinnerComponent } from './ui/loading-spinner/loading-spinner.component';
import { NgSelectModule } from '@ng-select/ng-select'


// @ts-ignore
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    CustomersComponent,
    BusinessComponent,
    MainComponent,
    PageNotFoundComponent,
    ServerErrorComponent,
    DocOfWhoWeAreComponent,
    DocOfOurTechnologiesComponent,
    DocOfRepresentativesComponent,
    DocOfJobsComponent,
    DocOfCollaborationComponent,
    DocOfServiceComponent,
    DocTermsOfUseComponent,
    DocOfFranchisingComponent,
    RegistrationComponent,
    ContactComponent,
    LoadingSpinnerComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    ModalModule.forRoot(),
    BsDropdownModule.forRoot(),
    NgSelectModule,

    // StoreModule.forRoot({listPage: listsReducer}),
    // StoreDevtoolsModule.instrument(),
  ],
  providers: [
    ContentService,
    { provide: AosToken, useValue: aos }],
  bootstrap: [AppComponent]
})
export class AppModule { }
