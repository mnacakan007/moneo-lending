import {Component, ElementRef, OnInit, Inject, Output, EventEmitter, HostListener, TemplateRef} from '@angular/core';
import {
  bounceInDownOnEnterAnimation,
  bounceInLeftOnEnterAnimation,
  bounceInOnEnterAnimation,
  bounceInRightOnEnterAnimation,
  bounceInUpOnEnterAnimation
} from '../../../lib/bouncing-entrances';
import {
  bounceOnEnterAnimation,
  flashOnEnterAnimation, jelloOnEnterAnimation,
  pulseOnEnterAnimation,
  rubberBandOnEnterAnimation, shakeOnEnterAnimation, swingOnEnterAnimation, tadaOnEnterAnimation, wobbleOnEnterAnimation
} from '../../../lib/attention-seekers';
import {
  flipInXOnEnterAnimation,
  flipInYOnEnterAnimation,
  flipOnEnterAnimation,
  flipOutXOnLeaveAnimation,
  flipOutYOnLeaveAnimation
} from '../../../lib/flippers';
import {
  bounceOutDownOnLeaveAnimation,
  bounceOutLeftOnLeaveAnimation,
  bounceOutOnLeaveAnimation, bounceOutRightOnLeaveAnimation,
  bounceOutUpOnLeaveAnimation
} from '../../../lib/bouncint-exits';
import {
  fadeInDownBigOnEnterAnimation,
  fadeInDownOnEnterAnimation, fadeInLeftBigOnEnterAnimation,
  fadeInLeftOnEnterAnimation,
  fadeInOnEnterAnimation, fadeInRightBigOnEnterAnimation, fadeInRightOnEnterAnimation, fadeInUpBigOnEnterAnimation,
  fadeInUpOnEnterAnimation
} from '../../../lib/fading-entrances';
import {
  fadeOutDownBigOnLeaveAnimation,
  fadeOutDownOnLeaveAnimation, fadeOutLeftBigOnLeaveAnimation,
  fadeOutLeftOnLeaveAnimation,
  fadeOutOnLeaveAnimation, fadeOutRightBigOnLeaveAnimation, fadeOutRightOnLeaveAnimation, fadeOutUpBigOnLeaveAnimation,
  fadeOutUpOnLeaveAnimation
} from '../../../lib/fading-exits';
import {lightSpeedInOnEnterAnimation, lightSpeedOutOnLeaveAnimation} from '../../../lib/light-speed';
import {
  rotateInDownLeftOnEnterAnimation, rotateInDownRightOnEnterAnimation,
  rotateInOnEnterAnimation,
  rotateInUpLeftOnEnterAnimation,
  rotateInUpRightOnEnterAnimation
} from '../../../lib/rotating-entrances';
import {
  rotateOutDownLeftOnLeaveAnimation, rotateOutDownRightOnLeaveAnimation,
  rotateOutOnLeaveAnimation,
  rotateOutUpLeftOnLeaveAnimation,
  rotateOutUpRightOnLeaveAnimation
} from '../../../lib/rotating-exits';
import {
  slideInDownOnEnterAnimation,
  slideInLeftOnEnterAnimation,
  slideInRightOnEnterAnimation,
  slideInUpOnEnterAnimation
} from '../../../lib/sliding-entrances';
import {
  slideOutDownOnLeaveAnimation,
  slideOutLeftOnLeaveAnimation,
  slideOutRightOnLeaveAnimation,
  slideOutUpOnLeaveAnimation
} from '../../../lib/sliding-exits';
import {
  zoomInDownOnEnterAnimation,
  zoomInLeftOnEnterAnimation,
  zoomInOnEnterAnimation, zoomInRightOnEnterAnimation,
  zoomInUpOnEnterAnimation
} from '../../../lib/zooming-entrances';
import {
  zoomOutDownOnLeaveAnimation,
  zoomOutLeftOnLeaveAnimation,
  zoomOutOnLeaveAnimation, zoomOutRightOnLeaveAnimation,
  zoomOutUpOnLeaveAnimation
} from '../../../lib/zooming-exits';
import {
  hingeOnLeaveAnimation,
  jackInTheBoxOnEnterAnimation,
  rollInOnEnterAnimation,
  rollOutOnLeaveAnimation
} from '../../../lib/specials';
import {
  collapseOnLeaveAnimation,
  expandOnEnterAnimation,
  fadeInExpandOnEnterAnimation,
  fadeOutCollapseOnLeaveAnimation
} from '../../../lib/other';
import {AosToken} from '../aos';
import {ContentService} from '../services/content.service';
import {log} from 'util';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {BsModalRef, BsModalService} from "ngx-bootstrap";
import {ActivatedRoute, Router, NavigationStart, NavigationEnd} from '@angular/router';

// import {List, Lists} from "../list.model";
// import {Store} from "@ngrx/store";
// import {AppState} from "../../store/app.state";
// import {Observable} from "rxjs";


@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss'],
  animations: [
    bounceInRightOnEnterAnimation({anchor: 'enter1'}),
    bounceInRightOnEnterAnimation({anchor: 'enter2', delay: 100}),
    bounceInRightOnEnterAnimation({anchor: 'enter3', delay: 200, animateChildren: 'none'}),
    bounceInLeftOnEnterAnimation({anchor: 'enter4'}),
    bounceInLeftOnEnterAnimation({anchor: 'enter5', delay: 100}),
    bounceOnEnterAnimation(),
    flashOnEnterAnimation(),
    pulseOnEnterAnimation(),
    rubberBandOnEnterAnimation(),
    shakeOnEnterAnimation(),
    swingOnEnterAnimation(),
    tadaOnEnterAnimation(),
    wobbleOnEnterAnimation(),
    jelloOnEnterAnimation(),
    flipOnEnterAnimation(),
    bounceInOnEnterAnimation(),
    bounceInUpOnEnterAnimation(),
    bounceOutOnLeaveAnimation(),
    bounceOutDownOnLeaveAnimation(),
    bounceInDownOnEnterAnimation(),
    bounceOutUpOnLeaveAnimation(),
    bounceInLeftOnEnterAnimation(),
    bounceInRightOnEnterAnimation(),
    bounceOutLeftOnLeaveAnimation(),
    bounceOutRightOnLeaveAnimation(),
    fadeInOnEnterAnimation(),
    fadeInUpOnEnterAnimation(),
    fadeInDownOnEnterAnimation(),
    fadeInLeftOnEnterAnimation(),
    fadeInRightOnEnterAnimation(),
    fadeInUpBigOnEnterAnimation(),
    fadeInDownBigOnEnterAnimation(),
    fadeInLeftBigOnEnterAnimation(),
    fadeInRightBigOnEnterAnimation(),
    fadeOutOnLeaveAnimation(),
    fadeOutUpOnLeaveAnimation(),
    fadeOutDownOnLeaveAnimation(),
    fadeOutLeftOnLeaveAnimation(),
    fadeOutRightOnLeaveAnimation(),
    fadeOutUpBigOnLeaveAnimation(),
    fadeOutDownBigOnLeaveAnimation(),
    fadeOutLeftBigOnLeaveAnimation(),
    fadeOutRightBigOnLeaveAnimation(),
    flipInXOnEnterAnimation(),
    flipInYOnEnterAnimation(),
    flipOutXOnLeaveAnimation(),
    flipOutYOnLeaveAnimation(),
    lightSpeedInOnEnterAnimation(),
    lightSpeedOutOnLeaveAnimation(),
    rotateInOnEnterAnimation(),
    rotateInUpLeftOnEnterAnimation(),
    rotateInUpRightOnEnterAnimation(),
    rotateInDownLeftOnEnterAnimation(),
    rotateInDownRightOnEnterAnimation(),
    rotateOutOnLeaveAnimation(),
    rotateOutUpLeftOnLeaveAnimation(),
    rotateOutUpRightOnLeaveAnimation(),
    rotateOutDownLeftOnLeaveAnimation(),
    rotateOutDownRightOnLeaveAnimation(),
    slideInRightOnEnterAnimation(),
    slideInUpOnEnterAnimation(),
    slideInDownOnEnterAnimation(),
    slideInLeftOnEnterAnimation(),
    slideOutUpOnLeaveAnimation(),
    slideOutDownOnLeaveAnimation(),
    slideOutLeftOnLeaveAnimation(),
    slideOutRightOnLeaveAnimation(),
    zoomInOnEnterAnimation(),
    zoomInUpOnEnterAnimation(),
    zoomInDownOnEnterAnimation(),
    zoomInLeftOnEnterAnimation(),
    zoomInRightOnEnterAnimation(),
    zoomOutOnLeaveAnimation(),
    zoomOutUpOnLeaveAnimation(),
    zoomOutDownOnLeaveAnimation(),
    zoomOutLeftOnLeaveAnimation(),
    zoomOutRightOnLeaveAnimation(),
    hingeOnLeaveAnimation(),
    jackInTheBoxOnEnterAnimation(),
    rollInOnEnterAnimation(),
    rollOutOnLeaveAnimation(),
    expandOnEnterAnimation({duration: 400}),
    collapseOnLeaveAnimation({duration: 400}),
    fadeInExpandOnEnterAnimation({duration: 400}),
    fadeOutCollapseOnLeaveAnimation({duration: 400})
  ]
})

export class MainComponent implements OnInit {
  //@Output() eventEmitterClick = new EventEmitter();

  public scrollTop: number = 0;
  public loginGroup: FormGroup;
  public bounceOutUp: any;
  public home = true;
  public business = false;
  public customers = false;
  public businessContent = false;
  public customersContent = true;
  public onBusiness = false;
  public onCustomers = true;
  public active_content: boolean;
  public active_content_text_customers = 0;
  public active_content_text_business = 0;
  public active_content_text_varius = 0;
  public active_illustrations_img = 0;
  public active_tariff = 'Armenia';
  public active_content_text_moneo: number;
  public imgMargin: boolean;
  public spangMargin: boolean;
  public selectedIndex = 0;
  public selectedVariusIndex: number;
  public selectedCircleIndex = 0;
  public selectedCircleButtonIndex = 0;
  public selected = false;
  public name: string;
  public text: string;
  public list_content: boolean;
  public listContent = [];
  public customersContentArray = [];
  public businessContentArray = [];
  public variusContentArray = [];
  public activeContentText: boolean;
  public styleExp = 'hidden';
  public illusForBusinesses2: string[] = [];
  public illusForCustomers2: string[] = [];
  public id: string;
  // public listState: Observable<Lists>;
  showSpinner: boolean = true;
  modalRef: BsModalRef;

  public lists: any[] = [
    {
      name: 'New & Only',
      text: 'MONEO is a NEW and the ONLY modern IT platform that includes all the main functions, tools and capabilities that are interesting and beneficial for both businesses and consumers. Useful information, discounts, bonuses, cash back,  modern payment systems, targeted advertising, loyalty programs, marketing tools, statistics, analytics and much more - all in a convenient program on your smart phone and on your computer. This is a new INNOVATIVE and UNIQUE program where the interests of businesses and consumers are perfectly balanced and everyone wins!',
    },
    {
      name: 'Global',
      text: 'Customers get extremely useful information via MONEO app, such as: interesting places around or anywhere',
    },
    {
      name: 'Affordable',
      text: 'Customers get extremely useful information via MONEO app, such as: interesting places around or anywhere',
    },
    {
      name: 'Fast & interactive',
      text: 'Customers get extremely useful information via MONEO app, such as: interesting places around or anywhere',
    },
    {
      name: 'Secure',
      text: 'Customers get extremely useful information via MONEO app, such as: interesting places around or anywhere',
    },
    {
      name: 'Dynamic',
      text: 'Customers get extremely useful information via MONEO app, such as: interesting places around or anywhere',
    },
    {
      name: 'Convenient for use',
      text: 'Customers get extremely useful information via MONEO app, such as: interesting places around or anywhere',
    },
    {
      name: 'Modern',
      text: 'Customers get extremely useful information via MONEO app, such as: interesting places around or anywhere',
    },
  ];

  public mainCustomers: string[] = [
    'Information & targeted offers',
    'Discounts, bonuses & cash back',
    'Contactless payments',
    'All cards on the smart phone',
    'Convenience & availability',
    'Incentives & various benefits',
    'Other modern tools',
  ];

  public mainBusiness: string[] = [
    'Information',
    'Discounts, bonuses & cash',
    'Contactless payments',
    'All cards on the smart phone',
    'Convenience',
    'Incentives',
    'Other modern tools',
  ];

  public businessTexts: string[] = [
    'Restaurant', 'Supermarket', 'Dental Clinic',
    'Diagnostic', 'Gas Station', 'Beauty Salon',
    'Gym', 'Electronics', 'Car Rent', 'Bank',
    'Insurance', 'Airport',
    'Hotel', 'Travel',
  ];

  public illusForCustomers: string[] = [
    '1. Download the MONEO App by scanning the QR code on this website or on any other platform; in places (restaurants, shops, cafes, etc.) that are included in the MONEO system, or directly from the App Store or Google Play Store.',
    '2. Get registered to get targeted offers.',
    '3. Learn the App; it is not difficult, although it provides many features and amenities.',
    '4. Check - what is available on the map?',
    '5. Subscribe to various businesses online or directly or directly on the spot - in a restaurant, cafe or shop, and get your discount, bonus or cash back cards.',
    '6. Attach your payment / credit cards to your profile to make secure online payments.',
    '7. Start using the MONEO app - find interesting places and offers, make secure payments and get your discounts, bonuses and/or cash back immediately - from the first visit!!!',
    '8. MONEO App is FREE for users!!!'
  ];

  public illusForBusinesses: string[] = [
    '1. Apply to MONEO for registration via this site.',
    '2. After signing the contract, you will get access to your separate secure account and dashboard under your login and password.',
    '3. If necessary, our experts will help you install additional programs and / or devices with you - on site or online.',
    '4. Start using MONEO - add information about your business, create customer groups, choose your loyalty program, add offers, adds, images, logo, contacts, links and more details to your profile. All will appear on MONEO App immediately.',
    '5. The system automatically generates a special QR code for your business, which you can save electronically and print.',
    '6. The system automatically provides you with standard advertising materials - flyers, stickers, with your QR code, logo and brief information about discounts, bonuses and other offers that you can place in your restaurant, cafe, shop or elsewhere. With one QR code scan, visitors will become your regular customers!',
    '7. You will have an e-wallet in order to give bonuses or cash back to your customers immediately after shopping, thereby increasing their loyalty to your business! So you will need to keep some minimal amount in your wallet (depends on your business) to ensure the smooth operation of your service and promises given to your customers.',
    '8. The prices and tariffs of the above-mentioned modern services of MONEO are very affordable, profitable and competitive for all businesses!'
  ];

  public states: Array<Object> = [
    {id: 1, name: 'Armenia'},
    // {id: 2, name: 'Russia'},
    // {id: 3, name: 'France'},
    // {id: 4, name: 'Belarus'},
  ];

  constructor(
    @Inject(AosToken) aos,
    private route: ActivatedRoute,
    public router: Router,
    private modalService: BsModalService,
    public contentService: ContentService,
    private eRef: ElementRef) {
    aos.init();
    router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        if (event.url == '/customers') {
          this.home = false;
          this.customers = true;
          this.customersContent = true;
          this.businessContent = false;
          this.onBusiness = false;
          this.onCustomers = true;
          this.styleExp = 'hidden';
        } else if (event.url == '/business') {
          this.home = false;
          this.business = true;
          this.businessContent = true;
          this.customersContent = false;
          this.onBusiness = true;
          this.onCustomers = false;
          this.styleExp = 'visible';
        } else if (event.url == '/home') {
          this.home = true;
          this.customers = false;
          this.business = false;
          this.styleExp = 'hidden';
          this.businessContent = false;
          this.customersContent = true;
          this.onCustomers = true;
          this.onBusiness = false;
          this.selectedIndex = 0;
          this.selectedCircleIndex = 0;
          this.selectedCircleButtonIndex = 0;
          this.active_illustrations_img = 0;
        }
      }
    });
  }

  ngOnInit() {
    // this.store.select('listPage').subscribe(({lists}) =>{
    //   this.lists = lists;
    // });
    // this.listState = this.store.select('listPage');

    const svgStates = document.querySelectorAll('#states > *');
    const myDIV = document.getElementById('info-box');
    const myState = document.getElementById('info-name');
    const after = document.getElementsByClassName('afterCircle');
    const status = document.getElementsByClassName('state-status');
    const wordStates = document.querySelectorAll('.word-states');
    const circleStates = document.querySelectorAll('.circle-link');
    const top = document.querySelectorAll('.top-nav');

    this.illusForCustomers2 = this.illusForCustomers2.concat(this.illusForCustomers);
    this.illusForBusinesses2 = this.illusForBusinesses2.concat(this.illusForBusinesses);

    this.contentService.changeActiveClass();

    this.loginGroup = new FormGroup({
      login: new FormControl('', [<any>Validators.required]),
      password: new FormControl('', [<any>Validators.required]),
    });

    setTimeout(() => {
      this.showSpinner = false;
    }, 3000);

    function addOnFromState(el) {
      svgStates.forEach(function () {
        // el.style.fill = el.style.fill === 'rgb(242, 242, 242)' ? 'rgb(251, 137, 102)' : 'rgb(242, 242, 242)';
        myDIV.style.display = 'inline-block';
        myDIV.innerHTML = el.getAttribute('data-info');
      });
    }

    svgStates.forEach(function (el) {
      el.addEventListener('click', function ($event: MouseEvent): void {
        addOnFromState(el);
        const ele = document.getElementById('mapBlock');
        window.scrollTo(ele.offsetLeft, ele.offsetTop - 100);
        if (status[0].innerHTML == 'Near Future') {
          Object(after[0]).style.backgroundColor = '#f8e71c';
        } else if (status[0].innerHTML == 'Active') {
          Object(after[0]).style.backgroundColor = 'rgb(251, 137, 102)';
        } else if (status[0].innerHTML == 'Coming Soon') {
          Object(after[0]).style.backgroundColor = '#b8e986';
        }
        const top = ($event.pageY - 185).toString();
        const left = ($event.pageX - 136).toString();
        myDIV.style.top = `${top}.px`;
        myDIV.style.left = `${left}.px`;
      });
    });

    window.addEventListener('click', $event => {
      if (!(Object($event.target).hasAttribute('data-circles'))) {
        if (this.illusForCustomers.length < 8) {
          this.illusForCustomers = [];
          this.illusForCustomers = this.illusForCustomers.concat(this.illusForCustomers2);
          this.selectedCircleButtonIndex = 0;
        }
        if (this.illusForBusinesses.length < 8) {
          this.illusForBusinesses = [];
          this.illusForBusinesses = this.illusForBusinesses.concat(this.illusForBusinesses2);
          this.selectedCircleButtonIndex = 0;
        }
      }

      if (!(Object($event.target).hasAttribute('data-name'))) {
        myDIV.style.display = 'none';
      }

      if (!(Object($event.target).hasAttribute('data-link'))) {
        this.deactivate();
      }

      if (!(Object($event.target).hasAttribute('data-links'))) {
        const class1 = document.getElementsByClassName('active_varius_button1');
        const class2 = document.getElementsByClassName('active_varius_button2');

        if (class1[0] == undefined) {
          return false;
        } else {
          Object(class1)[0].classList.remove('active_varius_button1');
        }

        if (class2[0] == undefined) {
          return false;
        } else {
          Object(class2)[0].classList.remove('active_varius_button2');
        }
      }
    });

    svgStates.forEach(function (el) {
      el.addEventListener('mouseenter', function () {
        myState.style.display = 'inline-block';
        myState.innerHTML = el.getAttribute('data-name');
      });
      el.addEventListener('mousemove', function ($event: MouseEvent): void {
        const top = ($event.pageY - 40).toString();
        const left = ($event.pageX - 35).toString();
        myState.style.top = `${top}.px`;
        myState.style.left = `${left}.px`;
      });
      el.addEventListener('mouseleave', function () {
        myState.style.display = 'none';
      });
    });

    function addOnFromList(el) {
      const stateCode = el.getAttribute('data-state');
      const svgState = document.querySelector('#' + stateCode);
      const info = svgState.getAttribute('data-name');
      svgStates.forEach(function (element) {
        if (element.getAttribute('data-name') == info) {
          myDIV.style.display = "inline-block";
          myDIV.innerHTML = svgState.getAttribute('data-info');
          const bodyRect = document.body.getBoundingClientRect();
          const elemRect = element.getBoundingClientRect();
          const positionTop = elemRect.top - bodyRect.top;
          const positionLeft = elemRect.left - bodyRect.left;
          let l, t;
          if (info == 'Russia') {
            t = (Math.floor(positionTop) - 140).toString();
            l = (Math.floor(positionLeft)).toString();
            myDIV.style.left = `${l}.px`;
            myDIV.style.top = `${t}.px`;
          } else {
            t = (Math.floor(positionTop) - 180).toString();
            l = (Math.floor(positionLeft) - 130).toString();
            myDIV.style.left = `${l}.px`;
            myDIV.style.top = `${t}.px`;
          }
        }
      });
    }

    wordStates.forEach(function (el) {
      el.addEventListener('click', function ($event: MouseEvent): void {
        const ele = document.getElementById('mapBlock');
        let left = ele.offsetLeft;
        let top = ele.offsetTop - 85;
        window.scrollTo({left, top, behavior: 'smooth'});
        addOnFromList(el);
      });

    });

    circleStates.forEach(function (el) {
      console.log(el);
      // el.addEventListener('click', function ($event: MouseEvent): void {
      //   console.log(el);
      //   // const ele = document.getElementById('circleBlock');
      //   // window.scrollTo(ele.offsetLeft, ele.offsetTop - 85);
      // })
    });

  }

  @HostListener('window:scroll', ['$event'])
  onWindowScroll($event) {
    this.scrollTop = $event.target.scrollingElement.scrollTop;
  }

  toHome() {
    this.home = true;
    this.business = false;
    this.customers = false;
    window.scrollTo(scrollX, 0);
  }

  onChangedContent() {
    if (this.customers || this.customersContent) {
      this.home = false;
      this.customers = false;
      this.business = true;
      this.customersContent = false;
      this.businessContent = true;
      this.onBusiness = true;
      this.onCustomers = false;
      this.selectedIndex = 0;
      this.selectedCircleIndex = 0;
      this.selectedCircleButtonIndex = 0;
      this.active_illustrations_img = 0;
      this.styleExp = 'visible';
    } else if (this.business || this.businessContent) {
      this.home = false;
      this.business = false;
      this.businessContent = false;
      this.customers = true;
      this.customersContent = true;
      this.onCustomers = true;
      this.onBusiness = false;
      this.selectedIndex = 0;
      this.selectedCircleIndex = 0;
      this.selectedCircleButtonIndex = 0;
      this.active_illustrations_img = 0;
      this.styleExp = 'hidden';
    }
  }

  changeStyle(template: TemplateRef<any>, $event, i: number, m) {
    if (document.documentElement.clientWidth >= 768) {
      const ele = document.getElementById('featuresBlock');
      const featuresSectionBottom = ele.getBoundingClientRect().top + window.pageYOffset;
      let left = 0;
      let top = featuresSectionBottom - 150;
      window.scrollTo({left, top, behavior: 'smooth'});
      this.active_content_text_customers = i;
      this.active_content_text_business = i;
      this.selectedIndex = i;
      this.active_content = $event.type === 'click' ? this.active_content = false : this.active_content = true;
    } else {
      this.active_content_text_customers = i;
      this.active_content_text_business = i;
      this.selectedIndex = i;
      this.customersContentArray = [];
      this.customersContentArray.push(i, m);
      this.businessContentArray = [];
      this.businessContentArray.push(i, m);
      this.active_content = $event.type === 'click' ? this.active_content = false : this.active_content = true;
      this.modalRef = this.modalService.show(template, {class: 'modal-lg modal-dialog-centered'});
    }
  }

  goDown() {
    const topSection = document.getElementById('topSectionBlock');
    const features = document.getElementById('featuresBlock');
    const various = document.getElementById('variousBlock');
    const circle = document.getElementById('circleBlock');
    const tariff = document.getElementById('tariffBlock');
    const map = document.getElementById('mapBlock');

    const topSectionTop = topSection.getBoundingClientRect().top + window.pageYOffset;
    const featuresSectionTop = features.getBoundingClientRect().top + window.pageYOffset;
    const variousSectionTop = various.getBoundingClientRect().top + window.pageYOffset;
    const circleSectionTop = circle.getBoundingClientRect().top + window.pageYOffset;
    const mapSectionTop = map.getBoundingClientRect().top + window.pageYOffset;
    const tariffSectionTop = tariff.getBoundingClientRect().top + window.pageYOffset;

    if (this.scrollTop <= 830) {
      let left = 0;
      let top = topSectionTop - 180;
      window.scrollTo({left, top, behavior: 'smooth'});
    }
    if (this.scrollTop <= featuresSectionTop - 150 && this.scrollTop >= 830) {
      let left = 0;
      let top = featuresSectionTop - 150;
      window.scrollTo({left, top, behavior: 'smooth'});
    }
    if (this.scrollTop >= featuresSectionTop - 150 && this.scrollTop <= variousSectionTop - 80) {
      let left = 0;
      let top = variousSectionTop - 80;
      window.scrollTo({left, top, behavior: 'smooth'});
    }
    if (this.scrollTop >= variousSectionTop - 80 && this.scrollTop <= circleSectionTop - 50) {
      let left = 0;
      let top = circleSectionTop - 50;
      window.scrollTo({left, top, behavior: 'smooth'});
    }
    if (this.scrollTop >= circleSectionTop - 50 && this.scrollTop <= mapSectionTop - 100) {
      let left = 0;
      let top = mapSectionTop - 100;
      window.scrollTo({left, top, behavior: 'smooth'});
    }
    if (this.scrollTop >= mapSectionTop - 100 && this.scrollTop <= tariffSectionTop + 100) {
      let left = 0;
      let top = tariffSectionTop + 100;
      window.scrollTo({left, top, behavior: 'smooth'});
    }
    if (this.scrollTop >= tariffSectionTop + 100) {
      let left = 0;
      let top = document.body.scrollHeight;
      window.scrollTo({left, top, behavior: 'smooth'});
    }
    if (this.business && !this.home || !this.customers) {
      if (this.scrollTop <= 830) {
        let left = 0;
        let top = topSectionTop - 180;
        window.scrollTo({left, top, behavior: 'smooth'});
      }
    }
  }

  goUp() {
    const topSection = document.getElementById('topSectionBlock');
    const features = document.getElementById('featuresBlock');
    const various = document.getElementById('variousBlock');
    const circle = document.getElementById('circleBlock');
    const map = document.getElementById('mapBlock');
    const tariff = document.getElementById('tariffBlock');

    const topSectionTop = topSection.getBoundingClientRect().top + window.pageYOffset;
    const featuresSectionTop = features.getBoundingClientRect().top + window.pageYOffset;
    const variousSectionTop = various.getBoundingClientRect().top + window.pageYOffset;
    const circleSectionTop = circle.getBoundingClientRect().top + window.pageYOffset;
    const mapSectionTop = map.getBoundingClientRect().top + window.pageYOffset;
    const tariffSectionTop = tariff.getBoundingClientRect().top + window.pageYOffset;

    if (this.scrollTop <= 830) {
      let left = 0;
      let top = 0;
      window.scrollTo({left, top, behavior: 'smooth'});
    }
    if (this.scrollTop >= topSectionTop - 180) {
      let left = topSection.offsetLeft;
      let top = topSectionTop - 180;
      window.scrollTo({left, top, behavior: 'smooth'});
    }
    if (this.scrollTop >= featuresSectionTop - 150) {
      let left = 0;
      let top = featuresSectionTop - 150;
      window.scrollTo({left, top, behavior: 'smooth'});
    }
    if (this.scrollTop >= variousSectionTop - 80) {
      let left = map.offsetLeft;
      let top = variousSectionTop - 80;
      window.scrollTo({left, top, behavior: 'smooth'});
    }
    if (this.scrollTop >= circleSectionTop - 50) {
      let left = scrollX;
      let top = circleSectionTop - 50;
      window.scrollTo({left, top, behavior: 'smooth'});
    }
    if (this.scrollTop >= mapSectionTop - 100) {
      let left = 0;
      let top = mapSectionTop - 100;
      window.scrollTo({left, top, behavior: 'smooth'});
    }
    if (this.scrollTop <= document.body.scrollHeight && this.scrollTop >= tariffSectionTop + 100) {
      let left = 0;
      let top = tariffSectionTop + 99;
      window.scrollTo({left, top, behavior: 'smooth'});
    }
  }

  variusContent(template: TemplateRef<any>, $event, i: number, b) {
    if (document.documentElement.clientWidth >= 768) {
      console.log(this.active_tariff)
      this.active_content_text_varius = i;
      this.selectedVariusIndex = i;
    } else {
      this.active_content_text_varius = i;
      this.selectedVariusIndex = i;
      this.variusContentArray = [];
      this.variusContentArray.push(i, b);
      this.modalRef = this.modalService.show(template, {class: 'modal-lg modal-dialog-centered'});
    }
  }

  activeContent(template: TemplateRef<any>, $event, i, content) {
    console.log(document.documentElement.clientWidth);
    if (document.documentElement.clientWidth >= 768) {
      if (!this.selected) {
        this.selected = true;
        this.listContent = [];
        this.listContent.push(content.name, content.text, i);
      } else if (this.selected && this.listContent.length > 0 && content.name !== this.listContent[0]) {
        this.deactivate();
        setTimeout(() => {
          this.selected = true;
          this.listContent = [];
          this.listContent.push(content.name, content.text, i);
        }, 500);
      }
    } else {
      this.listContent = [];
      this.listContent.push(content.name, content.text, i);
      this.modalRef = this.modalService.show(template, {class: 'modal-lg modal-dialog-centered'});
      console.log("ok");
    }

  }

  deactivate() {
    this.selected = false;
  }

  illustrations(event, el) {
    if (document.documentElement.clientWidth >= 992) {
      this.selectedCircleIndex = el;
      this.active_illustrations_img = el;
      const ele = document.getElementById('circleBlock');
      let left = ele.offsetLeft;
      let top = ele.offsetTop - 50;
      window.scrollTo({left, top, behavior: 'smooth'});
      if (el == 6) {
        if (this.businessContent) {
          let left = ele.offsetLeft;
          let top = ele.offsetTop + 90;
          window.scrollTo({left, top, behavior: 'smooth'});
        }
      }
      if (el == 7) {
        if (this.businessContent) {
          let left = ele.offsetLeft;
          let top = ele.offsetTop + 180;
          window.scrollTo({left, top, behavior: 'smooth'});
        }
        // if (this.customersContent) {
        //   window.scrollTo(ele.offsetLeft, ele.offsetTop);
        // }
      }
    } else if (document.documentElement.clientWidth < 992) {
      if (this.illusForCustomers.length < 8) {
        this.illusForCustomers = [];
        this.illusForCustomers = this.illusForCustomers.concat(this.illusForCustomers2);
      }
      if (this.illusForBusinesses.length < 8) {
        this.illusForBusinesses = [];
        this.illusForBusinesses = this.illusForBusinesses.concat(this.illusForBusinesses2);
      }

      this.selectedCircleIndex = 0;
      this.selectedCircleButtonIndex = el;
      this.active_illustrations_img = el;
      this.illusForCustomers = this.illusForCustomers.splice(el);
      this.illusForBusinesses = this.illusForBusinesses.splice(el);

    }
  }

  illustrationsText(event, el) {
    if (document.documentElement.clientWidth < 992) {
      this.selectedCircleIndex = el;
      this.selectedCircleButtonIndex = el;
      this.active_illustrations_img = el;
    }

  }

  loginUser(model: any, isValid: boolean) {
    if (isValid) {

    }
  }

}
