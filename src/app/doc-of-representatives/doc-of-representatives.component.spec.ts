import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocOfRepresentativesComponent } from './doc-of-representatives.component';

describe('DocOfRepresentativesComponent', () => {
  let component: DocOfRepresentativesComponent;
  let fixture: ComponentFixture<DocOfRepresentativesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DocOfRepresentativesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocOfRepresentativesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
