import {Component, ElementRef, EventEmitter, OnInit, Output, TemplateRef, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {BsModalRef, BsModalService} from "ngx-bootstrap";
import {Router} from "@angular/router";
import {PasswordValidation} from "../registration/registration.component";

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {

  @ViewChild('scrollMe') private myScrollContainer: ElementRef;
  @Output() onChanged = new EventEmitter<boolean>();
  public formGroup: FormGroup;
  public submitted = false;
  public success = false;
  state = true;
  modalRef: BsModalRef;

  constructor(private fb: FormBuilder,
              private modalService: BsModalService,
              public router: Router) {}

  ngOnInit() {
    this.formGroup = this.fb.group({
      name: ['', Validators.required],
      country: ['', Validators.required],
      telephone: [''],
      email: ['', [Validators.required, Validators.email]],
      message: ['', Validators.required],
    });

  }

  change(increased: any) {
    this.onChanged.emit(increased);
    this.state = !this.state;
  }

  get f() { return this.formGroup.controls; }

  onSubmit(template: TemplateRef<any>, model: any, isValid: boolean) {
    this.submitted = true;
    if (isValid) {
      this.success = true;
      console.log(model);
      this.modalRef = this.modalService.show(template, {class: 'modal-lg modal-dialog-centered'});
      setTimeout(() => {
        this.modalRef.hide();
        this.router.navigate(['/home']);
      }, 5000);
    }
  }


}
