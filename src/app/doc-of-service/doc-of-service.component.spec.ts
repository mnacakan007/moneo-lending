import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocOfServiceComponent } from './doc-of-service.component';

describe('DocOfServiceComponent', () => {
  let component: DocOfServiceComponent;
  let fixture: ComponentFixture<DocOfServiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DocOfServiceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocOfServiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
